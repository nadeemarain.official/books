//
//  Constants.swift
//  Books
//
//  Created by Nadeem Arain on 11/21/18.
//  Copyright © 2018 logics limited. All rights reserved.
//

import Foundation

struct Constants {
    
   static let isbn = ["9789000035526", "9789000036851", "9789025750022", "9789045116136"]

    
    struct urls {
        
        static let BOOK_BY_ISBN = "https://www.booknomads.com/api/v0/isbn/"
    }
    
}
