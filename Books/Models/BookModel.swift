//
//  BookModel.swift
//  Books
//
//  Created by Nadeem Arain on 11/21/18.
//  Copyright © 2018 logics limited. All rights reserved.
//

import Foundation

struct BookKey {
    
    static let bookName = "Title"
    static let authres = "Authors"
    static let autherName = "Name"
    static let bookCover = "CoverThumb"
    static let isbn = "ISBN"
}

struct BookModel {
    
    var bookName: String
    var authers: Array<AutherModel>
    var bookCover: String
    var isbn: String
    
    init(isbn: String,bookName: String, authers: Array<AutherModel>, bookCover: String) {
        
        self.bookName = bookName
        self.authers = authers
        self.bookCover = bookCover
        self.isbn = isbn
    }
    
    static func parseData(data: NSDictionary) -> BookModel {
        
        let authers = data.object(forKey: BookKey.authres) as! NSArray

        var authersArray: Array<AutherModel> = []
        
        for auther in authers {
            
            let autherDic = auther as! NSDictionary
            
            let autherModel = AutherModel.init(name: autherDic.object(forKey: BookKey.autherName) as! String)
            
            authersArray.append(autherModel)
        }
        
        let model = BookModel.init(isbn: data.object(forKey: BookKey.isbn) as! String, bookName: data.object(forKey: BookKey.bookName) as! String, authers: authersArray, bookCover: data.object(forKey: BookKey.bookCover) as! String)

        return model
    }
}

struct AutherModel {
    
    var name: String
    
    init(name: String) {
        
        self.name = name
    }
}
