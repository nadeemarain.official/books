//
//  InternetManager.swift
//  Books
//
//  Created by Nadeem Arain on 11/21/18.
//  Copyright © 2018 logics limited. All rights reserved.
//

import Foundation
import Alamofire

class InternetManager {
    
    static let shared = InternetManager()

    func fetchBooks(isbn: String, completion: @escaping (_ model: BookModel, _ error: Error?) -> Void){
        
        if !Reachability.isConnectedToNetwork() {
            // No Network found
            return
        }
        
        let completeStringURL = Constants.urls.BOOK_BY_ISBN + isbn
        let url = URL(string: completeStringURL)
        
          Alamofire.request(URLRequest(url: url!)).responseJSON { (response) in
            
            if response.result.isSuccess {
                
                let dataDic: NSDictionary = response.result.value as! NSDictionary
                
                let model = BookModel.parseData(data: dataDic)
                
                completion(model, response.error)

            }else {
                
            }
        }
    }
    
}



