//
//  BookCell.swift
//  Books
//
//  Created by Nadeem Arain on 11/21/18.
//  Copyright © 2018 logics limited. All rights reserved.
//

import Foundation
import UIKit

class BookCell: UITableViewCell {
    
    @IBOutlet weak var coverIV: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var autherLabel: UILabel!
    
    
}
