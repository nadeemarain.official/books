//
//  BookListVC.swift
//  Books
//
//  Created by Nadeem Arain on 11/21/18.
//  Copyright © 2018 logics limited. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class BookListVC: UITableViewController {
    
    var dataArray: Array<BookModel> = []
    var counter: Int = 0
    
    override func viewDidLoad() {
        
        self.navigationItem.title = "Books"
        self.tableView.tableFooterView = UIView()
        
        ProgressLoader.shared.startProgressLoaderAnimating()
        fetchBooksFromNetwork()
        
    }
    
    func fetchBooksFromNetwork() {
        
        InternetManager.shared.fetchBooks(isbn: Constants.isbn[self.counter]) { (model, error) in
            
            self.dataArray.append(model)
            self.counter += 1
            
            if self.counter != Constants.isbn.count {
                
                // I am going to hit this api 4 times its only because of I don't have listing api where I can get the array of book as I have only one api get book by isbn so it return single book so thats why I am hitting this api 4 times as i can make list you can understand.
                
                self.fetchBooksFromNetwork()
            }else {
                self.tableView.reloadData()
                ProgressLoader.shared.stopProgressLoaderAnimating()
            }
            
        }
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookCell") as! BookCell
        
        let model = dataArray[indexPath.row]
        
        cell.coverIV.image = model.bookCover.toImage()
        cell.nameLabel.text = model.bookName
        cell.autherLabel.text = model.authers.first?.name
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = storyBoard.instantiateViewController(withIdentifier: "BookDetailVC") as! BookDetailVC
        detailVC.bookModel = dataArray[indexPath.row]
        self.navigationController?.pushViewController(detailVC, animated:  true)
    }
    
    
}

extension String{
    func toImage() -> UIImage{
        let temp = self.components(separatedBy: ",")
        let dataDecoded : Data = Data(base64Encoded: temp[1], options:
            .ignoreUnknownCharacters)!
        let decodedimage = UIImage(data: dataDecoded)
        return decodedimage!
    }
    
    var htmlToAttributedString: NSAttributedString? {
        
        let htmlCSSString = "<style>" +
            "html *" +
            "{" +
            "font-size: 18pt !important;" +
            "color: #000000 !important;" +
            "font-family: Helvetica !important;" +
        "}</style> \(self)"
        
        guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
            return nil
        }
        
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
