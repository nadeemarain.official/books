//
//  DetailCell.swift
//  Books
//
//  Created by Nadeem Arain on 11/21/18.
//  Copyright © 2018 logics limited. All rights reserved.
//

import Foundation
import  UIKit

class DetailCell: UITableViewCell {
    
    @IBOutlet weak var bookIV: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
     @IBOutlet weak var autherLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
}
