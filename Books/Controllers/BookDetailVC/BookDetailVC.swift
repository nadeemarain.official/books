//
//  BookDetailVC.swift
//  Books
//
//  Created by Nadeem Arain on 11/21/18.
//  Copyright © 2018 logics limited. All rights reserved.
//

import Foundation
import UIKit

class  BookDetailVC: UITableViewController {
    
    var bookModel: BookModel?
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
        cell.bookIV.image = bookModel?.bookCover.toImage()
        cell.titleLabel.text = bookModel?.bookName
        cell.autherLabel.text = "Auther: " + (bookModel?.authers.first?.name)!
        cell.descriptionLabel.attributedText = "Met gemiddelde prestaties win je geen wedstrijd. Daarom is je onderscheiden zeer belangrijk. Te vaak kijken we hiervoor naar de directe omgeving en concurrenten. Hoe meer je hiernaar kijkt, hoe meer je erop gaat lijken. En dat is nu juist niet de bedoeling.<br> <br> Als je onderscheidend wilt zijn, kun je niet anders dan innoveren. En gelukkig is innovatie eigenlijk best makkelijk, zolang je je gezond verstand gebruikt. En combineert. Branchmarking staat voor het slim toepassen van branchevreemde best practices binnen je eigen organisatie om zo onderscheidend te worden. En te blijven.<br> <br> Antennecreativiteit is de spil van het boek. Want waarom steeds het wiel opnieuw uitvinden? De hele wereld is een grote boekenkast vol ideeÃ«n die wachten om opnieuw en elders gebruikt te worden. Je hoeft alleen de juiste ideeÃ«n er maar uit te pikken en toe te passen.<br> <br> In dit boek kom je veel best practices tegen uit diverse branches en landen. Je zult zien dat er veel te leren is van geschiedenis, biologie en andere wetenschappen.<br> <br> â€¢ Hoe verbreed je je inkomsten?<br> <br> â€¢ Hoe verdien je geld met het uitvergroten van je zwakke punt?<br> <br> â€¢ Hoe kun je mensen gratis voor je laten werken?<br> <br> â€¢ Hoe lever je de ultieme service?<br> <br> Dit boek stelt dat veel van wat je ooit hebt geleerd over innovatie onzin is. En dat je het lekker mag vergeten. Uiteindelijk gaat het maar om Ã©Ã©n ding: hoe kun je echt succesvol innoveren?<br> <br> <strong><em>Management Scope</em> geeft dit boek vier sterren!</strong><br> <br>".htmlToAttributedString
        
            cell.descriptionLabel.font = UIFont.systemFont(ofSize: 18.0)
        
        return cell
    }
}
